# server-config

Server configuration and administration

## Summary

### Domain name

`ouvre-boite.org` is rent from gandi.net. Automatic renewal is set up for June 9.

### Server requirements

> = 8Go RAM
> Debian 11 (End of support summer 2026)

### Configuration

Config files are listed in `conflist`. The script `dl-conf.sh` downloads every listed file and store them in the directory `conf`. This script should be run and followed by a commit after any modification to the server configuration.

Be sure not to list any file containing passwords in `conflist` !

## Installation checklist

- Upgrade the system.
- Create first user, set password for root, configure `sshd`
- Install git
- Install better shell (zsh + ohmyzsh)
- Create additional users
- Setup unattended upgrades

### Discourse

- Upgrade regularly (at least once a year), because after a long time, upgrades are no longer guaranteed to work.
- Discourse shares the port 443 with other websites, so the default embedded Let's Encrypt conf is not suited. See https://meta.discourse.org/t/running-other-websites-on-the-same-machine-as-discourse/17247. Additional documentation: https://meta.discourse.org/t/setting-up-https-support-with-lets-encrypt/40709, https://meta.discourse.org/t/advanced-setup-only-allowing-ssl-https-for-your-discourse-docker-setup/13847

### Mediawiki

- Use mariadb-server. (PostgreSQL support is experimental, and it won't interfere with other databases in the same server.)
- apache2 sometimes gets automatically installed with php. Be sure to uninstall it.
- Dump db, create the database, then import the dump (ee install instructions)
- [todo?] Install php-pear and set wgSMTP in LocalSettings.ini. pear install Mail and Net_SMTP.

### Landing page

Clone the `welcome` repo in /var/www/welcome/public and configure nginx.

## Maintenance

## Backups

- landing page : versionned on https://framagit.org/ouvreboite/welcome
- server configuration : versionned on https://framagit.org/ouvreboite/server-config
- discourse database : configured using the web admin interface to save weekly on AWS S3

## Yearly maintainance

- Backup mediawiki database and uploads
- Upgrade discourse
- Check debian end of support.
